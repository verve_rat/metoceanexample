#MetOcean Examples

A hosted example site can be found at: http://metocean-example.azurewebsites.net/


##Clone this repo:
* https: https://bitbucket.org/verve_rat/metoceanexample.git
* ssh: git@bitbucket.org:verve_rat/metoceanexample.git

###Building:
Run:
```
npm install
```
then:
```
browserify -d -t browserify-css ./site/main.js -o ./public/bundle.js
```

###Running the site locally:
```
npm start
```

##Structure:
The front end Javascript is defined in:
```
/site
```
and bundled to:
```
/public/bundle.js
```

The API is defined in:
```
/api/swagger/swagger.yaml
```
That definition is read by swagger-ui (installed in /public/swagger-docs/) and used to produce the docs at /swagger-docs.


The API is impletmented in:
```
/api/controllers/dataSet.js
/api/helpers/database.js
```