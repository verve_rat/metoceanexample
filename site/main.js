var d3 = require('d3');
var crossfilter = require('crossfilter');
var dc = require('dc');
var moment = require('moment');
var request = require('request');

//My charts
var timeLine = require('./timeline');
var waveHeightChart = require('./waveHeightChart');
var windRose = require('./windRose');

var css = require('./main.css');

var baseAPIURL = "http://" + window.location.hostname; 

//Wire up the reset link.
var resetLink = document.getElementById('reset-charts');
resetLink.addEventListener('click', resetCharts);

//Keep a handle on the main chart we will use to control the data.
var timelineChart = null;
//We also need to keep the metadata around for resetting.
var metadata = null;

function resetCharts() {
	if (timelineChart) {
		timelineChart.filter(null);
		dc.redrawAll();
	}
}

function onSelectedDateRangeChange(filter) {
	console.log('filter: ' + filter);
	var fromMoment = null;
	var toMoment = null;

	if (filter && filter.length > 1) {
		fromMoment = moment.utc(filter[0]);
		toMoment = moment.utc(filter[1]);
	}
	else {
		//We have removed the date range filter.
		fromMoment = moment.utc(metadata.earliestDataPointTime);
		toMoment = moment.utc(metadata.latestDataPointTime);
	}

	getWaveData(fromMoment, toMoment, waveHeightChart.changeData);
	drawWindRose(fromMoment, toMoment); //Just re-render from scratch. It's functional!
}

function drawWaveHeight(fromMoment, toMoment) {
	getWaveData(fromMoment, toMoment, function(data) {
		var chart = waveHeightChart.render({
			data: data,
			mountNode: "#waveHeight-chart"
		});
	});
}

function getWaveData(fromMoment, toMoment, onResponse) {

	//?fromDateTime=2014-02-10T05:00:00Z&toDateTime=2014-02-18T04:00:00Z
	var query = '?fromDateTime=' + fromMoment.format() + '&toDateTime=' + toMoment.format()

	request(baseAPIURL + '/prediction/wave' + query, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			var data = JSON.parse(body);
			onResponse(data);
		}
		else {
			onResponse([]);			
		}
	});
}


function getWindData(fromMoment, toMoment, onResponse) {

	//?fromDateTime=2014-02-10T05:00:00Z&toDateTime=2014-02-18T04:00:00Z
	var query = '?fromDateTime=' + fromMoment.format() + '&toDateTime=' + toMoment.format()

	request(baseAPIURL + '/prediction/wind' + query, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			var data = JSON.parse(body);
			onResponse(data);
		}
		else {
			onResponse([]);			
		}
	});
}

function drawWindRose(fromMoment, toMoment) {
	getWindData(fromMoment, toMoment, function(windData) {
		windRose.render(windData);
	});
}

//Get the full range of the available data and kick off rendering.
//If this was for real then we would restrict the date ranges to smaller subsets.
request(baseAPIURL + '/metadata', function(error, response, body) {
	if (!error && response.statusCode == 200) {
		metadata = JSON.parse(body);
		timelineChart = timeLine.render({
			earliestDataPointTime: metadata.earliestDataPointTime,
			latestDataPointTime: metadata.latestDataPointTime,
			mountNode: "#timeline-chart",
			onSelectedDateRangeChange: onSelectedDateRangeChange
		});

		drawWaveHeight(moment.utc(metadata.earliestDataPointTime), moment.utc(metadata.latestDataPointTime));
		drawWindRose(moment.utc(metadata.earliestDataPointTime), moment.utc(metadata.latestDataPointTime));
		
		dc.redrawAll();
	}
});