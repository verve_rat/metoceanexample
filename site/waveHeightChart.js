var d3 = require('d3');
var crossfilter = require('crossfilter');
var dc = require('dc');
var moment = require('moment');

module.exports = {
  render: renderWaveHeightChart,
  changeData: changeData
};

var waveHeightChart = null;
var dataset = null;
var dateDim = null;

function renderWaveHeightChart({data, mountNode}) {

	waveHeightChart = dc.compositeChart(mountNode);

	dataset = crossfilter(data);

	data.forEach(function(d) {
		d.date = moment.utc(d.time);
	});

	dateDim = dataset.dimension(function(d) { return moment.utc(d.time); });
	
	//hs : Significant wave height 
	//hx : Spectral estimate of maximum wave 
	
	var waveHeight = dateDim.group().reduceSum(function(d) { return d.hs; });
	var maxWave  = dateDim.group().reduceSum(function(d) { return d.hx; });

	var minDate = moment.utc(dateDim.bottom(1)[0].time);
	var maxDate = moment.utc(dateDim.top(1)[0].time);

	waveHeightChart
		.width(1000).height(200)
		.x(d3.time.scale.utc().domain([minDate, maxDate]))
		.yAxisLabel("Height (m)")
		.legend(dc.legend().x(50).y(5).itemHeight(13).gap(5))
		.renderHorizontalGridLines(true)
		.compose([
            dc.lineChart(waveHeightChart)
                .dimension(dateDim)
                .colors('red')
                .group(waveHeight, "Significant wave height")
                .dashStyle([2,2]),
            dc.lineChart(waveHeightChart)
                .dimension(dateDim)
                .colors('blue')
                .group(maxWave, "Spectral estimate of maximum wave")
                .dashStyle([5,5])
            ])
        .brushOn(false);
        //.render();
		
		//.dimension(dateDim)
		//.group(waveHeight)
		
	dc.renderAll();
	
	return waveHeightChart;
}

function changeData(newData) {
	//Clear all filters on this chart so when we remove data we will remove all data, not just the current view.
	waveHeightChart.filterAll();
	dataset.remove();
	dataset.add(newData);
	
	//Reset the scale of the chart to match the new range.
	var minDate = moment.utc(dateDim.bottom(1)[0].time);
	var maxDate = moment.utc(dateDim.top(1)[0].time);
	waveHeightChart.x(d3.time.scale.utc().domain([minDate, maxDate]))
	
	dc.redrawAll();
}