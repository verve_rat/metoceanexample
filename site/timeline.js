var d3 = require('d3');
var crossfilter = require('crossfilter');
var dc = require('dc');
var moment = require('moment');
require('moment-range');

module.exports = {
  render: renderTimelineChart
};

function renderTimelineChart({earliestDataPointTime, latestDataPointTime, mountNode, onSelectedDateRangeChange}) {
	var timelineChart = dc.lineChart(mountNode);
	
	//Construct a range of points by hour so we can fill in the timeline. 
	var timeRange = moment.range(moment.utc(earliestDataPointTime), moment.utc(latestDataPointTime));
	var timelinePoints = []; 
	
	timeRange.by('hours', function(moment) {
		  timelinePoints.push(moment);
	});
	
	var ndx = crossfilter(timelinePoints);
	
	var dateDim = ndx.dimension(function(d) { return moment.utc(d); });


	var byHour = dateDim.group(function(d) { return moment.utc(d).startOf('hour'); }); //.reduceSum(function(d) { return 1; });

	var minDate = moment.utc(dateDim.bottom(1)[0]);
	var maxDate = moment.utc(dateDim.top(1)[0]);

	timelineChart
		.width(1000).height(75)
		.dimension(dateDim)
		.group(byHour)
		.x(d3.time.scale.utc().domain([minDate, maxDate]))
		.mouseZoomable(true)
		//.yAxisLabel("nah (m)")
		.on("filtered", function(chart, filter){
			//We will want to set a timeout here because this function keeps firing while the user is draging the filters.
			onSelectedDateRangeChange(filter);
		});
	
	dc.renderAll();	

	return timelineChart;
}