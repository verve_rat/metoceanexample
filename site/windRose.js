//http://bl.ocks.org/bbest/2de0e25d4840c68f2db1
var d3 = require('d3');
var _ = require('lodash');
module.exports = {
	render: render
};

function render(data) {
	//data will look like:
	//[{wsp: 12, wd: 207}, {wsp: 22, wd: 92}]
	
	//Bucketize and transform.
	data.forEach(function(dataElement) {
		dataElement.directionBucket = dataElement.wd;
		dataElement.windSpeed = dataElement.wsp;
		dataElement.weight = 1;
	});

	//Transformed data: 
	// var windData = [
	// 	{
	// 		directionBucket: 0,
	// 		windSpeed: 70,
	// 		weight: 1

	// 	},
	// 	{
	// 		directionBucket: 1,
	// 		windSpeed: 30,
	// 		weight: 1
	// 	}
	// 	];

	//Wind data is displayed in a granularity of 1 degree.
	//So fill in any missing wind direction buckets.
	//Also: normalize the wind speed.
	var windData = [];
	var maxWindSpeed = 0;
	for (var i = 0; i < 360; i++) {

		var existingBucket = _.find(data, { 'directionBucket': i });

		if (existingBucket) {
			windData.push(existingBucket);
			if(existingBucket.windSpeed > maxWindSpeed) {
				maxWindSpeed = existingBucket.windSpeed;
			}
		}
		else {
			windData.push({
				directionBucket: i,
				windSpeed: 0,
				weight: 1
			});
		}
	}

	//Colour will be 100% green until windspeed is over 10, then will fade to red.
	var windSpeedTocolour = d3.scale.linear()
    	.domain([0, 10, 40])
    	.range(["#59E817", "#59E817", "red"]);
		
	windData.forEach(function(d) {
		d.color = windSpeedTocolour(d.windSpeed); 
		d.score = d.windSpeed / 30 * 100; //Normalize.
		d.width = 1;
	});

	var width = 500;
	var height = 500;
	var radius = Math.min(width, height) / 2;
	var innerRadius = 0.3 * radius;

	//Get a d3 pieChart layout-er
	var pieChart = d3.layout.pie()
		.sort(null)
		.value(function(d) { return d.width; });

	var arc = d3.svg.arc()
		.innerRadius(innerRadius)
		.outerRadius(function(d) {
			return (radius - innerRadius) * (d.data.score / 100.0) + innerRadius;
		});

	//Delete any old one hanging around.
	var svg = d3.select("#windRose-chart svg").remove();

	//Set up the root node for the graph.
	var svg = d3.select("#windRose-chart").append("svg")
		//.attr("id", "windRose")
		.attr("width", width)
		.attr("height", height)
		.append("g")
		.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

	//Fill in the segments with data.
	var path = svg.selectAll(".solidArc")
		.data(pieChart(windData))
		.enter().append("path")
		.attr("fill", function(d) { return d.data.color; })
		.attr("class", "solidArc")
		//.attr("stroke", "gray")
		.attr("d", arc);
	//Use an onClick ( .on('click', doFunction) maybe?) here to hook up selecting a segment to the external filtering.

	//Set up a magnitude ring.
	//TODO: figure out how to add a text label to that ring and figure out the value of that label.
	var magnitudeRingRadius = ((radius - innerRadius) /2) +  innerRadius; //Ring will be set at half the higest wind speed.
	var outlineArc = d3.svg.arc()
         .innerRadius(innerRadius)
         .outerRadius(magnitudeRingRadius);
	
	var outerPath = svg.selectAll(".outlineArc")
		.data(pieChart( [ {width: 1} ] )) //Fake data so we just get one big circle.
		.enter().append("path")
		.attr("fill", "none")
		.attr("stroke", "gray")
		.attr("class", "outlineArc")
		.attr("d", outlineArc);
		
	svg.append("svg:text")
		.attr("text-anchor", "middle")
		.attr("dy", ".35em")
		.text("Max wind speed: " + maxWindSpeed + "kts");
}