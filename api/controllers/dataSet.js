'use strict';
/*
 'use strict' is not required but helpful for turning syntactical errors into true errors in the program flow
 https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
*/

/*
 Modules make it possible to import JavaScript files into your application.  Modules are imported
 using 'require' statements that give you a reference to the module.

  It is a good idea to list the modules that your application depends on in the package.json in the project root
 */
var util = require('util');
var database = require('../helpers/database');
var moment = require('moment');

/*
 Once you 'require' a module you can reference the things that it exports.  These are defined in module.exports.

 For a controller in a127 (which this is) you should export the functions referenced in your Swagger document by name.

 Either:
  - The HTTP Verb of the corresponding operation (get, put, post, delete, etc)
  - Or the operationId associated with the operation in your Swagger document

  In the starter/skeleton project the 'get' operation on the '/hello' path has an operationId named 'hello'.  Here,
  we specify that in the exports of this module that 'hello' maps to the function named 'hello'
 */
module.exports = {
  fullDataSet: fullDataSet,
  metadata: metadata,
  getPrediction: getPrediction,  
  getPredictionWind: getPredictionWind,  
  getPredictionWave: getPredictionWave
};

/*
  Functions in a127 controllers used for operations should take two parameters:

  Param 1: a handle to the request object
  Param 2: a handle to the response object
 */
function fullDataSet(req, res) {
  //Variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
  //var name = req.swagger.params.name.value || 'stranger';
    
  var dataset = database.getFullDataSet();
  //This sends back a JSON response.
  res.json(dataset);
}

function metadata(req, res) {
  res.json(database.getMetadata());
}

function getPrediction(req, res) {
	var fromMoment = moment.utc(req.query.fromDateTime);
	var toMoment = moment.utc(req.query.toDateTime);
	
	var datasetForRange = database.getPredictions(fromMoment, toMoment);
	
	res.json(datasetForRange);
}

function getPredictionWind (req, res) {
	var fieldsToGet = ['time', 'wsp', 'wd'];
	var fromMoment = moment.utc(req.query.fromDateTime);
	var toMoment = moment.utc(req.query.toDateTime);
	
	var subSetForRange = database.getFields(fromMoment, toMoment, fieldsToGet);
	
	res.json(subSetForRange);
}

function getPredictionWave (req, res) {
	var fieldsToGet = ['time', 'hx', 'hs'];
	var fromMoment = moment.utc(req.query.fromDateTime);
	var toMoment = moment.utc(req.query.toDateTime);
	
	var subSetForRange = database.getFields(fromMoment, toMoment, fieldsToGet);
	
	res.json(subSetForRange);
}